<?php
 
class ParentIndexerType extends eZDataType
{
    const DATA_TYPE_STRING = 'parentindexer';

    function __construct()
    {
        parent::__construct( ParentIndexerType::DATA_TYPE_STRING, 'Parent Indexer' );
    }

    function isIndexable()
    {
        return true;
    }
 
    function metaData( $contentObjectAttribute )
    {
        $db = eZDB::instance();
        $object = $contentObjectAttribute->object();

		$parentnode = eZContentObjectTreeNode::fetch( $object->mainNode()->attribute('parent_node_id') );

		$indexArray = array();
        $indexArrayOnlyWords = array();

		$contentObject = $parentnode->attribute('object');
		$currentVersion = $contentObject->version($contentObject->attribute('current_version'));

		foreach ( $currentVersion->contentObjectAttributes() as $attribute )
		{
			$metaData = array();
			$classAttribute = $attribute->contentClassAttribute();
			if ( $classAttribute->attribute( "is_searchable" ) == 1 )
			{
				// Fetch attribute translations
				$attributeTranslations = $attribute->fetchAttributeTranslations();

				foreach ( $attributeTranslations as $translation )
				{
					$tmpMetaData = $translation->metaData();
					if( ! is_array( $tmpMetaData ) )
					{
						$tmpMetaData = array( array( 'id' => $attribute->attribute( 'contentclass_attribute_identifier' ),
													 'text' => $tmpMetaData ) );
					}
					$metaData = array_merge( $metaData, $tmpMetaData );
				}

				foreach( $metaData as $metaDataPart )
				{
					$trans = eZCharTransform::instance();
					$text = $trans->transformByGroup(  $metaDataPart['text'], 'search' );
					$text = str_replace( array( "\"", "*" ), array( " ", " " ), $text );

					// Split text on whitespace
					if ( is_numeric( trim( $text ) ) )
					{
						$integerValue = (int) $text;
					}
					else
					{
						$integerValue = 0;
					}
					$wordArray = split( " ", $text );

					foreach ( $wordArray as $word )
					{
						if ( trim( $word ) != "" )
						{
							$indexArray[] = array( 'Word' => $word,
												   'ContentClassAttributeID' => $attribute->attribute( 'contentclassattribute_id' ),
												   'identifier' => $metaDataPart['id'],
												   'integer_value' => $integerValue );
							$indexArrayOnlyWords[] = $word;
						}
					}
				}
			}
		}

        $indexArrayOnlyWords = array_unique( $indexArrayOnlyWords );
        $metaDataString = implode( $indexArrayOnlyWords, ' ' );

        return $metaDataString;
    }
}
 
eZDataType::register( ParentIndexerType::DATA_TYPE_STRING, 'parentindexertype' );
 
?>