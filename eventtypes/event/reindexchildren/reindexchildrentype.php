<?php
 
class ReindexChildrenType extends eZWorkflowEventType
{
    const WORKFLOW_TYPE_STRING = "reindexchildren";

	function ReindexChildrenType()
    {
        $this->eZWorkflowEventType( ReindexChildrenType::WORKFLOW_TYPE_STRING, ezpI18n::tr( 'kernel/workflow/event', "Reindex child nodes" ) );
        $this->setTriggerTypes( array( 'content' => array( 'publish' => array( 'after' ) ) ) );
    }
 
    function execute( $process, $event )
    {
        $parameters = $process->attribute( 'parameter_list' );
        $objectID = $parameters['object_id'];

		$db = eZDB::instance();

		$object = eZContentObject::fetch( $objectID );

		$subnodes = eZContentObjectTreeNode::subTreeByNodeID( array( 'Depth' => 1, 'Limitation' => array()), $object->mainNode()->attribute('node_id') );

        foreach( $subnodes as $subnode )
        {
        	$childNodeID = $subnode->attribute('main_node_id');

            /* Find object ID but only if the child's object contains searchable
               attribute of the parentindexer datatype. */
            $childObjectID = $db->arrayQuery( "SELECT a.contentobject_id
                                                  FROM ezcontentobject_tree t,
                                                       ezcontentobject_attribute a,
                                                       ezcontentclass_attribute ca
                                                  WHERE t.node_id=$childNodeID
                                                    AND t.contentobject_id=a.contentobject_id
                                                    AND t.contentobject_version=a.version
                                                    AND a.contentclassattribute_id=ca.id
                                                    AND ca.version=0
                                                    AND ca.data_type_string='parentindexer'
                                                    AND ca.is_searchable=1", array( 'limit' => 1 ) );
            if ( !$childObjectID )
            {
                continue;
            }

            $childObjectID = $childObjectID[0]['contentobject_id'];

            eZContentOperationCollection::registerSearchObject( $childObjectID, false );
            eZContentCacheManager::clearContentCache( $childObjectID );
        }
 
        return eZWorkflowType::STATUS_ACCEPTED;
    }
}
 
eZWorkflowEventType::registerEventType( ReindexChildrenType::WORKFLOW_TYPE_STRING, "ReindexChildrenType" );

?>
